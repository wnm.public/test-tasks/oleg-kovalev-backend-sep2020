const axios = require('axios')

module.exports = async(nickname) => 
axios.get(`https://overbuff.com/players/pc/${nickname.replace('#', '-')}`).then((res) => {
    if(res.status === 200) return true
}).catch(() => {
    return false
})