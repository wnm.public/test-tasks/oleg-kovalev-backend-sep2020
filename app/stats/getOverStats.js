const axios = require('axios')
const cheerio = require('cheerio')

module.exports = async(nickname) =>  
    axios.get(`https://overbuff.com/players/pc/${nickname.replace('#', '-')}`).then(res => { 

            const data = res.data

            const $ = cheerio.load(data)

            return { 
                username: nickname,
                avatarURL: $('.image-player').attr('src'),
                lvl: $('.corner').text()
        }
    })