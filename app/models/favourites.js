module.exports = (sequelize, Sequelize) => { 
    const Favourites = sequelize.define('favourites', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        favouriteNickname: {
            type: Sequelize.STRING,
            allowNull: false
        }
    })
    
    return Favourites
}