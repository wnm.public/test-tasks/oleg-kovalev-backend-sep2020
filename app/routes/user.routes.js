module.exports = app => {
    const users = require('../controllers/user.controllers')

    var router = require('express').Router()

    router.post("/register", users.create)

    router.post("/auth", users.auth)

    app.use('/', router)
}