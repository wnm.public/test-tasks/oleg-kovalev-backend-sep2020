module.exports = app => {
    const favourites = require('../controllers/favourite.controllers')
    const authToken = require('../auth/authToken')

    var router = require('express').Router()

    router.post("/addplayer", authToken, favourites.create)

    router.get('/getplayers', authToken, favourites.findAllFavourites)

    app.use('/api', router)
}