const db = require('../models')
const getOverStats = require('../stats/getOverStats')
const checkOverStats = require('../stats/checkOverStats')
const Favourites = db.favourites

exports.create = async(req, res) => {
    let nickname

    if(await checkOverStats(req.body.nickname)) {
        nickname = req.body.nickname
    } else {
        return res.status(404).send({ message: 'User not found' })
    } 

    const favourite = {
        userId: req.user.id,
        favouriteNickname: nickname
    }

    Favourites.create(favourite)
    .then(data => res.send(data))
    .catch(err => res.status(500).send({
         message: err.message
     }))
}

exports.findAllFavourites = (req, res) => {
    Favourites.findAll({ where: { userId: req.user.id } })
        .then(async data => {

            if(!data.length) return res.status(404).send({ message: 'У пользователя отсутствуют фавориты' })

            const users = []

            Promise.all(data.map(favourite => getOverStats(favourite.favouriteNickname)))
                .then(statsArr => {
                    statsArr.forEach(user => {
                        users.push(user);
                    })

                    res.json(users)
                })
            
        })
        .catch(err => res.status(500).send({
            message: err.message
        }))
}