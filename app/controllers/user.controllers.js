const bcrypt = require('bcrypt')
const generateAuthToken = require('../auth/generateAuthToken')

const db = require('../models')
const Users = db.users

const convertToBytes = (mb) => mb * 1024 * 1024

exports.create = async(req, res) => { 
    if(!req.body.email || !req.body.password) return res.status(400).send({ 
        message: 'Не указан email и/или пароль'
    })
    if(!req.body.firstname || !req.body.lastname) return res.status(400).send({
        message: 'Не указаны имя и/или фамилия'
    })
    if(!req.file) return res.status(400).send({
        message: 'Недопустимый тип файла или файл не указан'
    })
    if(req.file.size >= convertToBytes(5)) return res.status(400).send({
        message: 'Файл превышает допустимый вес(5mb)'
    })

    const hashedPassword = await bcrypt.hash(req.body.password, 10)
    
    const user = {
        firstName: req.body.firstname,
        lastName: req.body.lastname,
        email: req.body.email,
        password: hashedPassword,
        avatar: `avatars/${req.file.filename}`
    }

    const accessToken = await generateAuthToken(user)

    Users.create(user)
    .then(() => res.json(accessToken))
    .catch(err => res.status(500).send({
         message: err.message
     })) 
}

exports.auth = async (req, res) => {

    if(!req.body.email || !req.body.password) return res.status(400).send({ 
        message: 'Не указан email и/или пароль'
    })

    const userData = await Users.findOne({where: {
        email: req.body.email
    }})

    bcrypt.compare(req.body.password, userData.password, async(err, result)=> {
        if(!result) return res.status(400).send({
            message: 'Неправильный логин или пароль'
        })

        const { id, firstName, lastName, email, avatar } = userData

        const user = {
            id: id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            avatar: avatar
        }
        const accessToken = await generateAuthToken(user)
        res.json(accessToken)
    })
}
