require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const multer = require('multer')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

const storageConfig = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "avatars")
    },
    filename: (req, file, cb) => {
        cb(null, Math.random().toString(36).substring(7) + path.extname(file.originalname))
    }
})

const fileFilter = (req, file, cb) => { 
    if(file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg')
    {
        cb(null, true)
    }
    else{
        cb(null, false)
    }
}

app.use(multer({ storage: storageConfig, fileFilter: fileFilter }).single('avatar'))

const db = require('./app/models/index')
db.sequelize.sync()

app.use('/images', express.static('avatars'))

require("./app/routes/user.routes")(app)
require("./app/routes/favourite.routes")(app)

const port = process.env.PORT || 3000

app.listen(port, ()=> console.log(`Server working on port ${port}`))